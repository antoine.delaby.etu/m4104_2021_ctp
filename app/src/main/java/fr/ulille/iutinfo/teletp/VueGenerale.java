package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    // TODO Q1
    private String salle;
    private String poste;
    private String DISTANCIEL;
    // TODO Q2.c
    private SuiviViewModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        this.DISTANCIEL = getResources().getStringArray(R.array.list_salles)[0];
        this.poste = "";
        this.salle = this.DISTANCIEL;
        // TODO Q2.c
        this.model = MainActivity.model;
        // TODO Q4
        Spinner spSalle = (Spinner) view.findViewById(R.id.spSalle);
        ArrayAdapter adSalle = ArrayAdapter.createFromResource(view.getContext(), R.array.list_salles,android.R.layout.simple_spinner_dropdown_item);
        spSalle.setAdapter(adSalle);
        Spinner spPoste = (Spinner) view.findViewById(R.id.spPoste);
        ArrayAdapter adPoste = ArrayAdapter.createFromResource(view.getContext(), R.array.list_postes,android.R.layout.simple_spinner_dropdown_item);
        spSalle.setAdapter(adPoste);


        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
            TextView usn = view.findViewById(R.id.tvLogin);
            String username = usn.getText().toString();
            this.model.setUsername(username);
        });

        // TODO Q5.b
        update(view);
        //spSalle.setOnItemSelectedListener(view1 -> this.update(view1));
        // TODO Q9
    }

    // TODO Q5.a
    public void update(View view){
        Spinner spPoste = (Spinner) view.findViewById(R.id.spPoste);
        if(this.salle == this.DISTANCIEL){
            spPoste.setVisibility(View.GONE);
            spPoste.setEnabled(false);
            this.model.setLocalisation(DISTANCIEL);
        }else {
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(true);
            this.model.setLocalisation(this.salle + " : " + this.poste);
        }
    }
    // TODO Q9
}